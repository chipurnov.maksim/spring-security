package com.example.springsecurity.db

import javax.persistence.*

@Entity
@Table(name="dao_roles")
class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    var id: Long = 0

    @Column(name="name")
    var name: String = ""
}