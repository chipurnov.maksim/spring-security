package com.example.springsecurity.db

import javax.persistence.*

@Entity
@Table(name="dao_users")
class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    var id: Long = 0

    @Column(name="username")
    var username: String = ""

    @Column(name="pwd")
    var password: String = ""

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "dao_users_roles",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "role_id")]
    )
    var roles: Collection<Role> = listOf()
}