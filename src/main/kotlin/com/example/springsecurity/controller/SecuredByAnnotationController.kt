package com.example.springsecurity.controller

import com.example.springsecurity.service.ServiceWithSecureAnnotations
import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/")
@Profile("secureAnnotations")
class SecuredByAnnotationController(
    private val service: ServiceWithSecureAnnotations
) {

    @GetMapping("/secure")
    fun secure(principal: Principal) = service.secure(principal)

    @GetMapping("/user_page")
    fun userPage(principal: Principal) = service.userPage(principal)

    @GetMapping("/admin_page")
    fun adminPage(principal: Principal) = service.adminPage(principal)

    @GetMapping("/permission")
    fun permission() = service.permission()
}