package com.example.springsecurity.controller

import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/")
@Profile("antMatchers")
class SecuredByAntMatcherController {

    @GetMapping("/secure")
    fun secure(principal: Principal) =
        "secure page for ${principal.name} via ant matcher"

    @GetMapping("/user_page")
    fun userPage(principal: Principal) =
        "user page for ${principal.name} via ant matcher"

    @GetMapping("/admin_page")
    fun adminPage(principal: Principal) =
        "admin page for ${principal.name} via ant matcher"

    @GetMapping("/permission")
    fun permission() =
        "page for users with permission via ant matcher"
}