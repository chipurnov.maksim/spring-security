package com.example.springsecurity.service

import com.example.springsecurity.repository.UserRepository
import org.springframework.context.annotation.Profile
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Profile("dao")
class DaoUserService(
    private val userRepository: UserRepository
): UserDetailsService {
    @Transactional
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsername(username)
            ?: throw IllegalArgumentException("User $username not found.")
        val builder = org.springframework.security.core.userdetails.User.builder()
        val authorities = user.roles.map { SimpleGrantedAuthority(it.name) }
        return builder
            .username(user.username)
            .password(user.password)
            .authorities(authorities)
            .build()
    }
}