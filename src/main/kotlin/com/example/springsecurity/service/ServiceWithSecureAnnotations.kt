package com.example.springsecurity.service

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import java.security.Principal

@Service
class ServiceWithSecureAnnotations {
    @PreAuthorize("isAuthenticated()")
    fun secure(principal: Principal) =
        "secure page for ${principal.name} via annotation"

    @PreAuthorize("hasRole('USER')")
    fun userPage(principal: Principal) =
        "user page for ${principal.name} via annotation"

    @PreAuthorize("hasRole('ADMIN')")
    fun adminPage(principal: Principal) =
        "admin page for ${principal.name} via annotation"

    @PreAuthorize("hasAuthority('SOME_PERMISSION')")
    fun permission() =
        "page for users with permission via annotation"
}