package com.example.springsecurity.config
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource


@Configuration
class DataSourceConfig {

    @Value("\${datasource.url}")
    private lateinit var url: String

    @Value("\${datasource.username}")
    private lateinit var username: String

    @Value("\${datasource.password}")
    private lateinit var password: String

    @Value("\${datasource.driver-class-name}")
    private lateinit var driverClassName: String

    @Bean
    fun dataSource(): DataSource {
        val builder = DataSourceBuilder.create()

        return builder.apply {
            url(url)
            username(username)
            password(password)
            driverClassName(driverClassName)
        }.build()
    }
}