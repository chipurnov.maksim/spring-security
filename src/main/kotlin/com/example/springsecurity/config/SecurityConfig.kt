package com.example.springsecurity.config

import com.example.springsecurity.service.DaoUserService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.security.provisioning.JdbcUserDetailsManager
import org.springframework.security.provisioning.UserDetailsManager
import org.springframework.security.web.SecurityFilterChain
import javax.sql.DataSource


@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig(
    private val daoUserService: DaoUserService
) {

    @Bean
    @Profile("antMatchers")
    fun filterChainWithAntMatchers(http: HttpSecurity): SecurityFilterChain {
        return http
            .authorizeRequests()
            .antMatchers("/secure/**").authenticated()
            .antMatchers("/user_page/**").hasAnyRole("USER", "ADMIN")
            .antMatchers("/admin_page/**").hasAnyRole("ADMIN")
            .antMatchers("/permission/**").hasAuthority("SOME_PERMISSION")
            .antMatchers("/**").permitAll()
            .and().formLogin()
            .and().build()
    }

    @Bean
    @Profile("secureAnnotations")
    fun filterChainAnnotations(http: HttpSecurity): SecurityFilterChain {
        return http
            .authorizeRequests()
            .antMatchers("/public/**").permitAll()
            .antMatchers("/**").authenticated()
            .and().formLogin()
            .and().build()
    }

    @Bean
    @Profile("inMemory")
    fun inMemoryUserDetails(): UserDetailsManager {
        return InMemoryUserDetailsManager(getDefaultUsers())
    }

    @Bean
    @Profile("jdbc")
    fun jdbcUserDetails(dataSource: DataSource): JdbcUserDetailsManager {
        val users = getDefaultUsers()
        val manager = JdbcUserDetailsManager(dataSource)
        users.forEach { user ->
            if (manager.userExists(user.username)) manager.updateUser(user)
            else manager.createUser(user)
        }
        return manager
    }

    @Bean
    @Profile("dao")
    fun daoAuthentication(): DaoAuthenticationProvider {
        val authProvider = DaoAuthenticationProvider()
        authProvider.setPasswordEncoder(BCryptPasswordEncoder())
        authProvider.setUserDetailsService(daoUserService)
        return authProvider
    }

    private fun getDefaultUsers(): List<UserDetails> {
        val encryptedPassword = "{bcrypt}\$2y\$10\$fnAKHDbNzBB5twjoyGESTue/17STQdT9a3xVeFKivRRFxhETqc1TS"
        return listOf(
            User.builder()
                .username("user")
                .password(encryptedPassword)
                .roles("USER")
                .build(),
            User.builder()
                .username("user_with_permission")
                .password(encryptedPassword)
                .authorities("SOME_PERMISSION", "ROLE_USER")
                .build(),
            User.builder()
                .username("admin")
                .password(encryptedPassword)
                .authorities("SOME_PERMISSION", "ROLE_USER", "ROLE_ADMIN")
                .build(),
        )
    }
}