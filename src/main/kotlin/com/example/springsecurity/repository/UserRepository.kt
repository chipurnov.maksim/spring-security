package com.example.springsecurity.repository

import com.example.springsecurity.db.User
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
@Profile("dao")
interface UserRepository: JpaRepository<User, Long> {
    fun findByUsername(username: String): User?
}