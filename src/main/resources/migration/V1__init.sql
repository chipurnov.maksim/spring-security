CREATE TABLE IF NOT EXISTS USERS (
    USERNAME varchar(255) PRIMARY KEY,
    "PASSWORD" varchar(255),
    ENABLED boolean
);

CREATE TABLE IF NOT EXISTS AUTHORITIES (
    ID IDENTITY PRIMARY KEY,
    USERNAME varchar(255),
    AUTHORITY varchar(255),
    FOREIGN KEY (USERNAME) REFERENCES USERS(USERNAME)
);

CREATE TABLE IF NOT EXISTS dao_users (
    id IDENTITY PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    pwd VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS dao_roles (
    id IDENTITY PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS dao_users_roles (
    user_id BIGINT NOT NULL,
    role_id BIGINT NOT NULL,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (user_id) REFERENCES dao_users(id),
    FOREIGN KEY (role_id) REFERENCES dao_roles(id)
);

INSERT INTO dao_users(id, username, pwd)
VALUES (1, 'user', '$2y$10$fnAKHDbNzBB5twjoyGESTue/17STQdT9a3xVeFKivRRFxhETqc1TS' ),
       (2, 'admin', '$2y$10$fnAKHDbNzBB5twjoyGESTue/17STQdT9a3xVeFKivRRFxhETqc1TS' ),
       (3, 'user_with_permission', '$2y$10$fnAKHDbNzBB5twjoyGESTue/17STQdT9a3xVeFKivRRFxhETqc1TS' );
INSERT INTO dao_roles(id, name)
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER'),
       (3, 'SOME_PERMISSION');
INSERT INTO dao_users_roles (user_id, role_id)
VALUES ( 1, 2 ),
       ( 2, 1 ), ( 2, 2 ), ( 2, 3 ),
       ( 3, 2 ), ( 3, 3 );